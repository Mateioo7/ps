df = input("Give degree of freedom: ");

a1 = tcdf(0, df);
a2 = 1 - tcdf(0, df);

b1_1 = tcdf(1, df);
b1_2 = tcdf(-1, df);
b1 = b1_1 - b1_2;
b2 = 1 - b1;

alpha = input("Give alpha: ");
c = tinv(alpha, df);

beta = input("Give beta: ");
d = tinv(1-beta, df);

