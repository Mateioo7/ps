df1 = input("Give first degree of freedom: ");
df2 = input("Give second degree of freedom: ");

a1 = fcdf(0, df1, df2);
a2 = 1 - fcdf(0, df1, df2);

b1_1 = fcdf(1, df1, df2);
b1_2 = fcdf(-1, df1, df2);
b1 = b1_1 - b1_2;
b2 = 1 - b1;

alpha = input("Give alpha: ");
c = finv(alpha, df1, df2);

beta = input("Give beta: ");
d = finv(1-beta, df1, df2);