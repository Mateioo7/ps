# 1.Let X have one of the following distributions:X?N(?,?) (normal),
# X?T(n)(Student),X??2(n), orX?F(m,n) (Fischer). 
# Compute the following:
# a)P(X?0) and P(X?0);
# b)P(?1?X?1) and P(X??1 orX?1);
# c) the valuex?such thatP(X < x?) =?, for??(0,1) 
#(x?is called thequan-tileof order?);
# d) the valuex?such thatP(X > x?) =?, for??(0,1) 
#(x?is the quantile oforder 1??).
mu = input("Give mu: ");
sigma = input("Give sigma: ");

# a) P(X?0)
a1 = normcdf(0, mu, sigma);
# P(X?0)
a2 = 1 - normcdf(0, mu, sigma) + normpdf(0);

# b) P(X<=1)
b1_1 = normcdf(1, mu, sigma);
# P(X<=-1)
b1_2 = normcdf(-1, mu, sigma);
# P(?1?X?1)
b1 = b1_1 - b1_2;
# P(X??1 or X?1)
b2 = 1 - b1;

# c) the value x? such thatP(X < x?) =?, for??(0,1) 
alpha = input("Give alpha: ");
c = norminv(alpha, mu, sigma);

# d) the valuex?such thatP(X > x?) =?, for??(0,1)
beta = input("Give beta: ");
d = norminv(1-beta, mu, sigma);










