df = input("Give degree of freedom: ");

a1 = chi2cdf(0, df);
a2 = 1 - chi2cdf(0, df);

b1_1 = chi2cdf(1, df);
b1_2 = chi2cdf(-1, df);
b1 = b1_1 - b1_2;
b2 = 1 - b1;

alpha = input("Give alpha: ");
c = chi2inv(alpha, df);

beta = input("Give beta: ");
d = chi2inv(1-beta, df);