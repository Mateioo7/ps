p = input("Give probability (in (0, 1)): ");
N = input("Give number of simulations: ");

for i=1:N
  X(i) = 0;
  while (rand >= p)
    X(i) = X(i) + 1;
  endwhile
endfor

unique_values = unique(X);
unique_values_frequencies = histc(X, unique_values);
unique_values_probabilities = unique_values_frequencies / N;

geo_values = 0:30; # my choice
geo_distr = geopdf(geo_values, p);

plot(geo_values, geo_distr, '.', 'markersize', 20, unique_values, unique_values_probabilities, 'o', 'markersize', 10);
