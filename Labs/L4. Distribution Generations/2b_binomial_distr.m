n = input("Give number of trials: ");
p = input("Give probability (in (0, 1)): ");
N = input("Give number of simulations: ");

for i = 1:N;
  U = rand(1, n); # vector of n values in (0, 1)
  X(i) = sum(U < p);
endfor

unique_values = unique(X);
unique_values_frequencies = histc(X, unique_values);
unique_values_probabilities = unique_values_frequencies / N;

bino_values = 0:n;
bino_distr = binopdf(bino_values, n, p);

plot(bino_values, bino_distr, '.', 'markersize', 20, unique_values, unique_values_probabilities, 'o', 'markersize', 10);