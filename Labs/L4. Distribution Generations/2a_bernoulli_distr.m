p = input("Give probability (in (0, 1)): ");
N = input("Give number of simulations: ");

# rand(1, N) -> vector of N random values in (0, 1)
# rand(N, M) -> matrix of NxM random values in (0, 1)
U = rand(1, N);

X = U;
# X(i) = 1, U < p (success); X(i) = 0, U >= p (failure)
X = U < p;

unique_values = unique(X)
unique_values_frequencies = histc(X, unique_values)
unique_values_probabilities = unique_values_frequencies / N


