##############################################
################# Problem B2 #################
##############################################
disp("Problem B2.");
disp("------------------------------------------");
X1 = [22.4, 21.7, 24.5, 23.4, 21.6, 23.3, 22.4, 21.6, 24.8, 20.0];
x1_mean = mean(X1); # sample average
x1_variance = var(X1); # sample variance
n1 = length(X1); # sample size

X2 = [17.7, 14.8, 19.6, 19.6, 12.1, 14.8, 15.4, 12.6, 14.0, 12.2];
x2_mean = mean(X2); # sample average
x2_variance = var(X2); # sample variance
n2 = length(X2); # sample size

conf_coeff = input("Give confidence coefficient (e.g. 0.95): "); # = 1 - alpha in (0, 1); try for 0.95
alpha = 1 - conf_coeff; # significance level

disp("------------------------------------------");
# a).
# quantile
q = tinv(1 - alpha / 2, n1 + n2 - 2); # formula from conf_int.pdf

# populations variance
s_p = sqrt( ((n1 - 1) * x1_variance + (n2 - 1) * x2_variance) / (n1 + n2 - 2) );

lower_bound = x1_mean - x2_mean - q * s_p * sqrt(1 / n1 + 1 / n2); # formula from conf_int.pdf
upper_bound = x1_mean - x2_mean + q * s_p * sqrt(1 / n1 + 1 / n2); # formula from conf_int.pdf

fprintf("a) The confidence interval, when sigma1 = sigma2 (unknowns), for the diff. of true means is: (%.3f, %.3f).\n", lower_bound, upper_bound);


# b).
# c, n - variables from conf_int.pdf formulas
c = (x1_variance / n1) / (x1_variance / n1 + x2_variance / n2);
n = 1 / (c^2 / (n1 - 1) + (1 - c)^2 / (n2 - 1));
# quantile
q = tinv(1 - alpha / 2, n);

lower_bound = x1_mean - x2_mean - q * sqrt(x1_variance / n1 + x2_variance / n2); # formula from conf_int.pdf
upper_bound = x1_mean - x2_mean + q * sqrt(x1_variance / n1 + x2_variance / n2); # formula from conf_int.pdf

fprintf("b) The confidence interval, when sigma1 != sigma2 (unknowns), for the diff. of true means is: (%.3f, %.3f).\n", lower_bound, upper_bound);


# c).
# quantiles
q1 = finv(1 - alpha / 2, n1 - 1, n2 - 1); # formula from conf_int.pdf
q2 = finv(alpha / 2, n1 - 1, n2 - 1); # formula from conf_int.pdf

lower_bound = (1 / q1) * (x1_variance / x2_variance); # formula from conf_int.pdf
upper_bound = (1 / q2) * (x1_variance / x2_variance); # formula from conf_int.pdf

fprintf("c) The confidence interval for the ratio of variances is: (%.3f, %.3f).\n\n", lower_bound, upper_bound);











