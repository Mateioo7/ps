##############################################
################# Problem B1 #################
##############################################
disp("Problem B1.");
# given sample for B
X = [7 7 4 5 9 9 4 12 8 1 8 7 3 13 2 1 17 7 12 5 6 2 1 13 14 10 2 4 9 11 3 5 12 6 10 7];
x_mean = mean(X); # sample average
n = length(X); # sample size

conf_coeff = input("Give confidence coefficient (e.g. 0.95): "); # = 1 - alpha in (0, 1); try for 0.95
alpha = 1 - conf_coeff; # significance level

disp("------------------------------------------");
# a).
sigma = 5; # given

# quantiles
q1 = norminv(1 - alpha / 2, 0, 1); # formula from conf_int.pdf
q2 = norminv(alpha / 2, 0, 1); # formula from conf_int.pdf

lower_bound = (x_mean - (sigma / sqrt(n)) * q1); # formula from conf_int.pdf
upper_bound = (x_mean - (sigma / sqrt(n)) * q2); # formula from conf_int.pdf

fprintf("a) Confidence interval, when sigma is known, for the data mean is: (%.3f, %.3f).\n", lower_bound, upper_bound);


# b).
# quantiles
q1 = tinv(1 - alpha / 2, n - 1); # formula from conf_int.pdf
q2 = tinv(alpha / 2, n - 1); # formula from conf_int.pdf

s = std(X); # standard deviation (s), function from lab teacher

lower_bound = (x_mean - (s / sqrt(n)) * q1); # formula from conf_int.pdf
upper_bound = (x_mean - (s / sqrt(n)) * q2); # formula from conf_int.pdf

fprintf("b) The confidence interval, when sigma is unknown, for the data mean is: (%.3f, %.3f).\n", lower_bound, upper_bound);


# c).
# quantiles
q1 = chi2inv(1 - alpha / 2, n - 1); # formula from conf_int.pdf
q2 = chi2inv(alpha / 2, n - 1); # formula from conf_int.pdf

s = std(X); # standard deviation (s), function from lab teacher
variance = s^2; # variance (s^2)
# st. dev. = sqrt(variance); variance is st. dev. squared

lower_bound = ((n - 1) * variance / q1); # formula from conf_int.pdf
upper_bound = ((n - 1) * variance / q2); # formula from conf_int.pdf

fprintf("c) The confidence interval, when data is approx. normally distributed, for the variance is: (%.3f, %.3f). ", lower_bound, upper_bound);
fprintf("For the standard deviation is: (%.3f, %.3f).\n\n", sqrt(lower_bound), sqrt(upper_bound));



