# given samples
# 22 * ones(1, 3) = 22 22 22
X = [20 * ones(1, 2) 21 22 * ones(1, 3) 23 * ones(1, 6) 24 * ones(1, 5) 25 * ones(1, 9) 26 * ones(1, 2) 27 * ones(1, 2)];
Y = [75 * ones(1, 3) 76  * ones(1, 2) 77 * ones(1, 2) 78 * ones(1, 5) 79 * ones(1, 8) 80 * ones(1, 8) 81 82];

# a).
x_mean = mean(X);
y_mean = mean(Y);
fprintf("a) Mean of X is %.3f and mean of Y is %.3f.\n", x_mean, y_mean);

# b).
x_variance = var(X);
y_variance = var(Y);
fprintf("b) Variance of X is %.3f and variance of Y is %.3f.\n", x_variance, y_variance);

# c).
covariance = cov(X, Y);
fprintf("c) Covariance of X and Y is %.3f.\n", covariance);

# d).
disp("d) Correlation coefficient of X and Y is:");
correlation_coefficient = corrcoef(X, Y)







