X = [46 37 39 48 47 44 35 31 44 37]; # given data 1
Y = [35 33 31 35 34 30 27 32 31 31]; # given data 2

alpha = 0.05 ; # given significance level

x_length = length(X);
y_length = length(Y);

disp("Subpoint a.")
disp("Hypothesis to test:")
disp("H0: null hypothesis when the variance of the standard method = variance of the new method.");
disp("H1: research hypothesis when the variance of the standard method != variance of the new method.\n");

# h1 contains variance(X) != variance(Y) -> both-tailed test
#             variance(X) > variance(Y) -> right-tailed test
#             variance(X) < variance(Y) -> left-tailed test


[h0, p, ci, stats] = vartest2(X, Y, "alpha", alpha, "tail", "both");
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that vartest2 doesn't reject H0 in favor of the H1. Therefore, the population variances are equal.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that vartest2 rejects H0 in favor of the H1. Therefore, the population variances are unequal.\n"); 
endif

fprintf("P-value of the test P: %.4f\n", p);
fprintf("Observed test statistic TS0: %.4f\n", stats.fstat);

upper_bound = finv(alpha / 2, x_length - 1, y_length - 1);
lower_bound = finv(1 - alpha / 2, x_length - 1, y_length - 1);
fprintf("Rejection region RR: (-Inf, %.4f) U (%.4f, Inf)\n", upper_bound, lower_bound);




fprintf("\n\n");
disp("Subpoint b.")

x1_variance = var(X);
x2_variance = var(Y);

n1 = length(X);
n2 = length(Y);

x1_mean = mean(X);
x2_mean = mean(Y);

if (h0 == 0)
  q = tinv(1 - alpha / 2, n1 + n2 - 2); # formula from conf_int.pdf

  # populations variance
  s_p = sqrt( ((n1 - 1) * x1_variance + (n2 - 1) * x2_variance) / (n1 + n2 - 2) );

  lower_bound = x1_mean - x2_mean - q * s_p * sqrt(1 / n1 + 1 / n2); # formula from conf_int.pdf
  upper_bound = x1_mean - x2_mean + q * s_p * sqrt(1 / n1 + 1 / n2); # formula from conf_int.pdf

  fprintf("The confidence interval, when sigmas for the difference of the average assembling times is: (%.3f, %.3f).\n", lower_bound, upper_bound);
elseif (h0 == 1)
  c = (x1_variance / n1) / (x1_variance / n1 + x2_variance / n2);
  n = 1 / (c^2 / (n1 - 1) + (1 - c)^2 / (n2 - 1));
  # quantile
  q = tinv(1 - alpha / 2, n);

  lower_bound = x1_mean - x2_mean - q * sqrt(x1_variance / n1 + x2_variance / n2); # formula from conf_int.pdf
  upper_bound = x1_mean - x2_mean + q * sqrt(x1_variance / n1 + x2_variance / n2); # formula from conf_int.pdf

  fprintf("The confidence interval, when sigmas are unequal, for the difference of the average assembling times is: (%.3f, %.3f).\n", lower_bound, upper_bound);
endif



