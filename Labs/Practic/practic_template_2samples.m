X = [22.4 21.7 24.5 23.4 21.6 23.3 22.4 21.6 24.8 20.0]; # given data 1
Y = [17.7 14.8 19.6 19.6 12.1 14.8 15.4 12.6 14.0 12.2]; # given data 2

alpha = 0.05 ; # given significance level

x_length = length(X);
y_length = length(Y);

disp("Subpoint a.")
disp("Hypothesis to test:")
disp("H0: null hypothesis when the variance of   (X) = variance of   (Y).");
disp("H1: research hypothesis when the variance of  (X) ? variance  (X).\n");

# h1 contains variance(X) ? variance(Y) -> both-tailed test
#             variance(X) > variance(Y) -> right-tailed test
#             variance(X) < variance(Y) -> left-tailed test


[h0, p, ci, stats] = vartest2(X, Y, "alpha", alpha, "tail", "both");
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that vartest2 doesn't reject H0 in favor of the H1. Therefore, H0 is valid.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that vartest2 rejects H0 in favor of the H1. Therefore, H1 is valid.\n"); 
endif

fprintf("P-value of the test P: %.4f\n", p);
fprintf("Observed test statistic TS0: %.4f\n", stats.fstat);

upper_bound = finv(alpha / 2, x_length - 1, y_length - 1);
lower_bound = finv(1 - alpha / 2, x_length - 1, y_length - 1);
fprintf("Rejection region RR: (-Inf, %.4f) U (%.4f, Inf)\n", upper_bound, lower_bound);


fprintf("\n\n");
disp("Subpoint b.")

if (h0 == 0)
  var_type = "equal";
elseif (h0 == 1)
  var_type = "unequal";
endif

fprintf("Hypothesis to test when the variances are %s (based on subpoint a):\n", var_type);
disp("H0: null hypothesis when the mean of  (X) = mean of  (Y).");
disp("H1: research hypothesis when the mean of   (X) <insert_sign> mean of  (Y).\n");

# h1 contains mean(X) ? mean(Y) -> both-tailed test
#             mean(X) > mean(Y) -> right-tailed test
#             mean(X) < mean(Y) -> left-tailed test

tail_type = "right"; # "both" / "right" / "left"

[h0, p, ci, stats] = ttest2(X, Y, "alpha", alpha, "tail", tail_type, "vartype", var_type);
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that ttest2 doesn't reject H0 in favor of the H1. Therefore, H0 is valid.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that ttest2 rejects H0 in favor of the H1. Therefore, H1 is valid.\n"); 
endif

fprintf("P-value of the test P: %d\n", p);
fprintf("Observed test statistic TS0: %.4f\n", stats.tstat);

n = x_length + y_length - 2;
bound = tinv(1 - alpha, n);
fprintf("Rejection region RR: (%.4f, Inf)\n", bound);

#{
# MAKE SURE SIGMAS ARE UNKNOWN
if (var_type == "equal")
  n = x_length + y_length - 2;
elseif (var_type == "unequal")
  x_variance = var(X);
  y_variance = var(Y);
  c = (x_variance / x_length) / (x_variance / x_length + y_variance / y_length);
  n = 1 / (c^2 / (x_length - 1) + (1 - c)^2 / (y_length - 1));
endif

if (tail_type == "right")
  bound = tinv(1 - alpha, n);
  fprintf("Rejection region RR: (%.4f, Inf)\n", bound);
elseif (tail_type == "left")
  bound = tinv(alpha, n);
  fprintf("Rejection region RR: (-Inf, %.4f)\n", bound);
elseif (tail_type == "both")
  bound1 = tinv(alpha / 2, n);
  bound2 = tinv(1 - alpha/2, n);
  fprintf("Rejection region RR: (-Inf, %.4f) U (%.4f, Inf)\n", bound1, bound2);
endif
#}








