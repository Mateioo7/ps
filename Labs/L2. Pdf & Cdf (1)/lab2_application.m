% simulate 3 coin tosses
# f) Write a Matlab code that simulates 3 coin tosses and computes the value of the
# variable X.
U = rand(1, 3);
# X = no. of heads that appear <=> no. of values that belong in [0, 0.5) range
Y = (U < 0.5);
X = sum(Y);

# a) Find the probability distribution function of X. What type of 
# distribution does X have?
# Binomial distribution.

# b) Find the cumulative distribution function of X, Fx.
b = binocdf(X, 3, 0.5)

# c) Find P(X = 0) and P(X != 1).
# P(X = 0)
c1 = binopdf(0, 3, 0.5)

bpdf2 = binopdf(1, 3, 0.5);
# P(X != 1) = 1 - P(X = 1)
c2 = 1 - bpdf2

# d) Find P(X ? 2), P(X < 2).
d1 = binocdf(2, 3, 0.5)

# P(X = 2)
d2 = binopdf(2, 3, 0.5);
# P(X < 2) = P(X <= 2) - P(X = 2)
d3 = d1 - d2

# e) Find P(X ? 1), P(X > 1).
e1 = 1 - binocdf(1, 3, 0.5);
# P(X ? 1) = (1 - P(X <= 1)) + P(X = 1)
e2 = e1 + bpdf2

# P(X > 1) = 1 - P(X <= 1)
e3 = e1



hist(X);