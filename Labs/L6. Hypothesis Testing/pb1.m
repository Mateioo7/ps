alpha = input("Give significance level (e.g. 0.05): ");
X = [7 7 4 5 9 9 4 12 8 1 8 7 3 13 2 1 17 7 12 5 6 2 1 13 14 10 2 4 9 11 3 5 12 6 10 7]; # given data
sigma = 5; # given sigma
miu0 = 9; # miu0 = given test value
# 1.a)
disp("\nProblem 1.a)");
disp("Hypothesis to test:")
fprintf("H0: null hypothesis when the mean of storaged files/computer = 9.\n");
fprintf("H1: research hypothesis when the mean of storaged files/computer < 9.\n\n");
# h1 contains "<" operator => left-tailed test
[h0, p, ci, zval] = ztest(X, miu0, sigma, "alpha", alpha, "tail", "left");
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that ztest doesn't reject H0 in favor of the H1. Therefore, the data suggests that the standard of 9 storaged files/computer is met.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that ztest rejects H0 in favor of the H1. Therefore, the data suggests that the standard of 9 storaged files/computer is not met.\n"); 
endif

fprintf("P-value of the test P: %d\n", p);
fprintf("Observed test statistic TS0: %d\n", zval);

upper_bound = norminv(alpha, 0, 1);
fprintf("Critical/rejection region RR: (-Inf, %d)\n", upper_bound);

# 1.b)
fprintf("---------------------------------------------------\n");
disp("Problem 1.b)");
disp("Hypothesis to test:")
disp("H0: null hypothesis when the mean of storaged files/computer = 5.5.");
disp("H1: research hypothesis when the mean of storaged files/computer > 5.5.\n");
miu0 = 5.5; # new given test value
# h1 contains ">" operator => right-tailed test
[h0, p, ci, stats] = ttest(X, miu0, "alpha", alpha, "tail", "right");
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that ttest doesn't reject H0 in favor of the H1. Therefore, the data suggests that the mean of storaged files/computer is equal to 5.5.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that ttest rejects H0 in favor of the H1. Therefore, the data suggests that the the mean of storaged files/computer exceeds 5.5.\n"); 
endif

fprintf("P-value of the test P: %d\n", p);
fprintf("Observed test statistic TS0: %d\n", stats.tstat);

n = length(X);
lower_bound = tinv(1 - alpha, n - 1);
fprintf("Critical/rejection region RR: (%d, Inf)\n", lower_bound);















