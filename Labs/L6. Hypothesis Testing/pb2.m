disp("Problem 2.a)");
disp("Hypothesis to test:")
disp("H0: null hypothesis when the variance of premium gas = variance of regular gas.");
disp("H1: research hypothesis when the variance of premium gas != variance of regular gas.\n");
# variance = sigma^2 - unknown
X = [22.4 21.7 24.5 23.4 21.6 23.3 22.4 21.6 24.8 20.0]; # given data 1
Y = [17.7 14.8 19.6 19.6 12.1 14.8 15.4 12.6 14.0 12.2]; # given data 2
alpha = 0.05 ; # given significance level

n1 = length(X);
n2 = length(Y);

# h1 contains "!=" operator => both-tailed test
[h0, p, ci, stats] = vartest2(X, Y, "alpha", alpha, "tail", "both");
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that vartest2 doesn't reject H0 in favor of the H1. Therefore, H0 is valid.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that vartest2 rejects H0 in favor of the H1. Therefore, H1 is valid.\n"); 
endif

fprintf("P-value of the test P: %.4f\n", p);
fprintf("Observed test statistic TS0: %.4f\n", stats.fstat);

upper_bound = finv(alpha / 2, n1 - 1, n2 - 1);
lower_bound = finv(1 - (alpha / 2), n1 - 1, n2 - 1);
fprintf("Critical/rejection region RR: (-Inf, %.4f) U (%.4f, Inf)\n", upper_bound, lower_bound);

fprintf("---------------------------------------------------\n");
disp("Problem 2.b)");
#disp("Hypothesis to test for equal variances (based on a)):")
disp("H0: null hypothesis when the mean of premium gas mileage = mean of regular gas mileage.");
disp("H1: research hypothesis when the mean of premium gas mileage > mean of regular gas mileage.\n");

if (h0 == 0)
  output = "equal";
elseif (h0 == 1)
  output = "unequal";
endif

# h1 contains ">" operator => right-tailed test
[h0, p, ci, stats] = ttest2(X, Y, "alpha", alpha, "tail", "right", "vartype", output);
if (h0 == 0)
  disp("The returned value of H0 = 0 indicates that ttest2 doesn't reject H0 in favor of the H1. Therefore, H0 is valid.\n"); 
elseif (h0 == 1)
  disp("The returned value of H0 = 1 indicates that ttest2 rejects H0 in favor of the H1. Therefore, H1 is valid.\n"); 
endif

fprintf("P-value of the test P: %d\n", p);
fprintf("Observed test statistic TS0: %.4f\n", stats.tstat);

n = n1 + n2 - 2;
lower_bound = tinv(1 - alpha, n);
fprintf("Critical/rejection region RR: (%.4f, Inf)\n", lower_bound);

# if the variances would have been different, we would have had:
#x_variance = var(X);
#y_variance = var(Y);
#disp("Comment after:");
#c = (x_variance / n1) / (x_variance / n1 + y_variance / n2);
#n = 1 / (c^2 / (n1 - 1) + (1 - c)^2 / (n2 - 1));
#lower_bound = tinv(1 - alpha, n);
#fprintf("Critical/rejection region RR: (%d, Inf)\n", lower_bound);








